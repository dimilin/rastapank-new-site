export type HerakleionDiyData = {
  id: string
  url: string
  site: string
  logo: string
}
