export type InfoData = {
  tel: string
  displayTel: string
  email: string
  facebook: string
  gitlab: string
  github: string
}
