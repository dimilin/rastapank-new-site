export type ChatData = {
  id: string
  url: string
  text: string
}
