export type AutopilotData = {
  id: number
  day: string
  title: string
  schedule: { from_time: string; name: string }[]
}
