export type ShowData = {
  id: number
  title: string
  subTitle?: string
  description?: string
  subtitle?: string
  image: string
  src?: string
  producers?: string
  facebook?: string
  instagram?: string
  twitter?: string
  email?: string
}
