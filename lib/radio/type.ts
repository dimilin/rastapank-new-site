export type RadioData = {
  id: number
  title: string
  subTitle?: string
  image?: string
  url?: string
  src: string
}
