/** @type {import('tailwindcss').Config} */
module.exports = {
  darkMode: ["class"],
  content: ["./pages/**/*.{ts,tsx}", "./components/**/*.{ts,tsx}", "./app/**/*.{ts,tsx}", "./src/**/*.{ts,tsx}"],
  theme: {
    container: {
      center: true,
      padding: "2rem",
      screens: {
        "2xl": "1400px",
      },
    },
    extend: {
      colors: {
        footer: "var(--footer-primary)",
        "footer-dark": "var(--footer-dark)",
        nav: "var(--nav-primary)",

        "link-primary": "var(--link-primary)",
        "link-secondary": "var(--link-secondary)",

        "fancy-primary": "var(--fancy-primary)",
        "fancy-white": "var(--fancy-white)",
        "fancy-black": "var(--fancy-black)",

        "title-dark": "var(--title-dark)",
        description: "var(--description)",

        border: "hsl(var(--border))",
        input: "hsl(var(--input))",
        ring: "hsl(var(--ring))",
        background: "hsl(var(--background))",
        foreground: "hsl(var(--foreground))",
        primary: {
          DEFAULT: "hsl(var(--primary))",
          foreground: "hsl(var(--primary-foreground))",
        },
        secondary: {
          DEFAULT: "hsl(var(--secondary))",
          foreground: "hsl(var(--secondary-foreground))",
        },
        destructive: {
          DEFAULT: "hsl(var(--destructive))",
          foreground: "hsl(var(--destructive-foreground))",
        },
        muted: {
          DEFAULT: "hsl(var(--muted))",
          foreground: "hsl(var(--muted-foreground))",
        },
        accent: {
          DEFAULT: "hsl(var(--accent))",
          foreground: "hsl(var(--accent-foreground))",
        },
        popover: {
          DEFAULT: "hsl(var(--popover))",
          foreground: "hsl(var(--popover-foreground))",
        },
        card: {
          DEFAULT: "hsl(var(--card))",
          foreground: "hsl(var(--card-foreground))",
        },
      },
      fontFamily: {
        sans: ["var(--font-inter)"],
      },
      borderRadius: {
        lg: "var(--radius)",
        md: "calc(var(--radius) - 2px)",
        sm: "calc(var(--radius) - 4px)",
      },
      boxShadow: {
        fancy: "0 5px 15px -5px #ecf0f1",
        "fancy-blue": "0 5px 15px -5px #3c85d5",
        "music-card": "0 8px 28px -9px rgba(0,0,0,.45)",
        "radio-card": "0 6px 10px -4px rgba(0,0,0,.15)",
        "radio-card-hover": "0 12px 19px -7px rgba(0,0,0,.3)",
      },
      scale: { fancy: "--tw-scale-x: 1.2; --tw-scale-y: 1.2;" },
      keyframes: {
        "accordion-down": {
          from: { height: 0 },
          to: { height: "var(--radix-accordion-content-height)" },
        },
        "accordion-up": {
          from: { height: "var(--radix-accordion-content-height)" },
          to: { height: 0 },
        },
        "fancy-button": {
          "0%": {
            left: "-110%",
            top: "90%",
          },
          "50%": {
            left: "10%",
            top: "-30%",
          },
          "100%": {
            left: "-10%",
            top: "-10%",
          },
        },
      },
      animation: {
        "accordion-down": "accordion-down 0.2s ease-out",
        "accordion-up": "accordion-up 0.2s ease-out",
        "fancy-button": "fancy-button .7s ease-in-out",
      },
      transitionDuration: {
        card: "0.3s, 0.2s, 0.3s",
      },
      transitionTimingFunction: {
        card: "cubic-bezier(0.34, 2, 0.6, 1), ease, cubic-bezier(0.34, 2, 0.6, 1)",
      },
      transitionProperty: {
        card: "transform, box-shadow",
      },
    },
  },
  plugins: [require("tailwindcss-animate")],
}
