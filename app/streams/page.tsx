import Footer from "@/components/footer"
import { Streams } from "@/lib/streams/type"
import PocketBase from "pocketbase"

async function getData() {
  const pb = new PocketBase("http://127.0.0.1:8090")

  const streams = await pb.collection("streams").getFullList<Streams>({
    sort: "created",
  })

  return streams.at(0)
}

export default async function Page() {
  const data = await getData()

  if (!data) return null

  return (
    <>
      <div className="py-16 font-light">
        <div className="flex flex-col  justify-center w-1/3 ml-auto mr-auto">
          <div className="flex flex-col gap-7 w-full">
            <div className="text-4xl text-[##66615b] text-center">Alternative streams</div>
            <div className="text-[#9a9a9a]">{data.description}</div>
            <div className="text-[#9a9a9a] text-center" dangerouslySetInnerHTML={{ __html: data.links }} />
          </div>
        </div>
      </div>
      <Footer />
    </>
  )
}
