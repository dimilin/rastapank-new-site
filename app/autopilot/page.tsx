import { AutopilotTimeline } from "@/components/autopilotTimeline"
import Footer from "@/components/footer"
import { Tabs, TabsContent, TabsList, TabsTrigger } from "@/components/ui/tabs"
import { AutopilotData } from "@/lib/autopilot/type"
import PocketBase from "pocketbase"

async function getData() {
  const pb = new PocketBase("http://127.0.0.1:8090")

  const records = await pb.collection("schedule_autopilot").getFullList<AutopilotData>({
    sort: "created",
  })

  return records
}

export default async function Page() {
  const data = await getData()

  if (!data) return null

  const days = data.map((dato) => ({ day: dato.title, value: dato.day }))

  return (
    <>
      <div className="py-16 font-light">
        <div className="flex flex-col gap-12 justify-center w-full text-center">
          <div className="text-4xl text-[##66615b] ml-auto mr-auto ">Πρόγραμμα</div>
          <Tabs defaultValue={data.at(0)?.day}>
            <TabsList>
              {data.map((dato) => (
                <TabsTrigger key={dato.day} value={dato.day}>
                  {dato.title}
                </TabsTrigger>
              ))}
            </TabsList>
            {data.map((dato) => {
              return (
                <TabsContent key={dato.day} value={dato.day}>
                  <AutopilotTimeline schedule={dato.schedule} />
                </TabsContent>
              )
            })}
          </Tabs>
        </div>
      </div>
      <Footer />
    </>
  )
}
