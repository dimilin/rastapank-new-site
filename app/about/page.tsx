import { FancyButton } from "@/components/ui/fancyButton"
import { TextLink } from "@/components/ui/textLink"
import { AboutUsData } from "@/lib/about/type"
import { InfoData } from "@/lib/type"
import { Facebook, Github, Gitlab } from "lucide-react"
import PocketBase from "pocketbase"

import dynamic from "next/dynamic"

const DynamicMap = dynamic(() => import("@/components/map"), {
  ssr: false,
})

async function getData() {
  const pb = new PocketBase("http://127.0.0.1:8090")

  const info = (await pb.collection("info").getList<InfoData>()).items.at(0)
  const text = (await pb.collection("about").getFullList<AboutUsData>()).at(0)?.presentationText

  return { text, info }
}

export default async function Page() {
  const data = await getData()

  if (!data.info) return null

  return (
    <div className="py-16 font-light">
      <div className="flex flex-col justify-center items-center w-1/3 ml-auto mr-auto">
        <div className="flex flex-col gap-16">
          <div className="flex flex-col gap-5">
            <div className="text-4xl text-title-dark">About Us</div>
            <div className="text-lg text-description" dangerouslySetInnerHTML={{ __html: `${data.text}` }} />
          </div>
          <div className="flex flex-col gap-5">
            <div className="text-4xl text-title-dark text-center">Βρείτε μας</div>
            <div className="text-lg text-description text-center flex flex-col gap-8">
              <div>
                <div className="font-semibold">ΔΙΕΥΘΥΝΣΗ</div>
                <div className="p-2 font-light">Πανεπιστημιούπολη Βουτών, Φοιτητικό Κέντρο</div>
                <div className="pt-2">
                  <DynamicMap />
                </div>
              </div>
              <div>
                <div className="font-semibold">ΕΠΙΚΟΙΝΩΝΙΑ</div>
                <div className="flex flex-col gap-y-1 p-2">
                  <span>
                    studio: <TextLink href={`callto:${data.info.tel}`}>{data.info.displayTel}</TextLink>
                  </span>
                  <span>
                    email: <TextLink href={`mailto:${data.info.email}`}>{data.info.email}</TextLink>
                  </span>
                  <div className="flex items-center justify-center gap-7 pt-2">
                    <FancyButton href={data.info.facebook}>
                      <Facebook />
                    </FancyButton>
                    <FancyButton href={data.info.github}>
                      <Github />
                    </FancyButton>
                    <FancyButton href={data.info.gitlab}>
                      <Gitlab />
                    </FancyButton>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}
