import { Card } from "@/components/card"
import Footer from "@/components/footer"
import { ShowData } from "@/lib/show/type"
import PocketBase from "pocketbase"

async function getData() {
  const pb = new PocketBase("http://127.0.0.1:8090")

  const shows = await pb.collection("shows").getFullList<ShowData>({
    sort: "created",
  })

  const data = shows.map((record) => {
    return { ...record, src: pb.files.getUrl(record, record?.image || "") }
  })

  return data
}

export default async function Page() {
  const data = await getData()

  if (!data) return null

  return (
    <>
      <div className="py-16 font-light">
        <div className="flex p-5 w-full">
          <div className="flex flex-col gap-12 w-full">
            <div className="text-4xl text-[##66615b] ml-auto mr-auto ">Εκπομπές</div>
            <div className="flex flex-wrap justify-center gap-10 w-full">
              {data.map((item) => {
                return (
                  <Card
                    key={item.id}
                    id={item.id}
                    src={item.src}
                    title={item.title}
                    subTitle={item.subTitle}
                    url={`/shows/${item.id}`}
                  />
                )
              })}
            </div>
          </div>
        </div>
      </div>
      <Footer />
    </>
  )
}
