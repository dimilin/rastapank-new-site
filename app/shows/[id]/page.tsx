import Footer from "@/components/footer"
import { FancyButton } from "@/components/ui/fancyButton"
import { ShowData } from "@/lib/show/type"
import { Facebook, Instagram, Mail, Twitter } from "lucide-react"
import Image from "next/image"
import PocketBase from "pocketbase"

async function getData(id: string) {
  const pb = new PocketBase("http://127.0.0.1:8090")

  const show = await pb.collection("shows").getOne<ShowData>(id)

  return { ...show, src: pb.files.getUrl(show, show?.image || "") }
}

export default async function Page({ params }: { params: { id: string } }) {
  const data = await getData(params.id)

  if (!data) return null

  return (
    <>
      <div className="flex flex-col justify-center items-center pb-32 gap-4">
        <div className="flex flex-col gap-5 text-center z-20">
          <div
            style={{ width: "170px", height: "170px" }}
            className="relative object-cover mt-[-85px] mx-auto mb-0 inline-block "
          >
            <Image
              src={data.src}
              alt={data.title}
              fill
              style={{ objectFit: "cover" }}
              className=" border-[5px] border-white rounded-[50%] p-0"
            />
          </div>
          <div className="flex flex-col gap-6">
            <div className="text-2xl text-title-dark font-light">{data.title}</div>
            <div className="text-xl text-description font-semibold">ΕΚΠΟΜΠΗ</div>
          </div>
        </div>
        {(data.facebook || data.twitter || data.instagram || data.email) && (
          <div className="flex items-center gap-6">
            {data.facebook && (
              <FancyButton href={data.facebook}>
                <Facebook />
              </FancyButton>
            )}
            {data.instagram && (
              <FancyButton href={data.instagram}>
                <Instagram />
              </FancyButton>
            )}
            {data.twitter && (
              <FancyButton href={data.twitter}>
                <Twitter />
              </FancyButton>
            )}
            {data.email && (
              <FancyButton href={`mailto:${data.email}`}>
                <Mail />
              </FancyButton>
            )}
          </div>
        )}
        <div
          className="text-lg text-center font-light w-[50%]"
          dangerouslySetInnerHTML={{ __html: data.description || "" }}
        />
        <div className="flex flex-col gap-1 pt-4 text-center">
          <div className="text-2xl font-light">Μουσικοί παραγωγοί</div>
          <div className="text-lg ">{data.producers}</div>
        </div>
      </div>
      <Footer />
    </>
  )
}
