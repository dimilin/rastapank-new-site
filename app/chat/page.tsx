import Footer from "@/components/footer"
import { TextLink } from "@/components/ui/textLink"
import { ChatData } from "@/lib/chat/type"
import PocketBase from "pocketbase"

async function getData() {
  const pb = new PocketBase("http://127.0.0.1:8090")
  const resultList = (await pb.collection("chat").getList<ChatData>()).items.at(0)
  return resultList
}

export default async function Page() {
  const data = await getData()

  if (!data) return null

  return (
    <>
      <div className="py-16 font-light">
        <div className="flex flex-col justify-center w-1/3 ml-auto mr-auto">
          <div className="flex flex-col gap-5">
            <div className="text-4xl text-title-dark">Chat</div>
            <div className="text-lg">
              To live chat βρίσκεται στο κανάλι <TextLink href={data.url}>{data.text}</TextLink>. Θα χρειαστεί να
              εγγραφείτε δίνοντας username/password για να συμμετέχετε στη κουβέντα.
            </div>
          </div>
        </div>
      </div>
      <Footer />
    </>
  )
}
