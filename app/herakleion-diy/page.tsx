import { HerakleionDiyData } from "@/lib/herakleion-diy/types"
import dynamic from "next/dynamic"
import PocketBase from "pocketbase"

const DynamicGallery = dynamic(() => import("@/components/gallery"), {
  ssr: false,
})

async function getData() {
  const pb = new PocketBase("http://127.0.0.1:8090")

  const records = await pb.collection("herakleionDIY").getFullList<HerakleionDiyData>({
    sort: "-created",
  })

  const data = records.map((record) => {
    return { ...record, src: pb.files.getUrl(record, record.logo) }
  })

  return data
}

export default async function Page() {
  const data = await getData()

  if (!data) return null

  return (
    <div className="py-16 font-light">
      <div className="flex flex-col justify-center w-1/2 ml-auto mr-auto">
        <div className="flex flex-col gap-7">
          <div className="text-4xl text-title-dark">Herakleion DIY affiliate projects</div>
          <DynamicGallery data={data} />
        </div>
      </div>
    </div>
  )
}
