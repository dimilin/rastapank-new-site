import { AutopilotData } from "@/lib/autopilot/type"
import { cn } from "@/lib/utils"
import styles from "./AutopilotTimeline.module.css"

export const AutopilotTimeline = (props: Pick<AutopilotData, "schedule">) => {
  const { schedule } = props
  if (!schedule.length) {
    return (
      <div className="p-5 justify-content-center">
        <h4 className="p-4">Δεν υπάρχει πρόγραμμα</h4>
      </div>
    )
  }

  return (
    <div className="m-5 justify-content-center">
      <ul className={cn(styles["timeline"], styles["timeline-centered"])}>
        {schedule.map((dato, i) => (
          <li className={styles["timeline-item"]} key={`${dato.from_time}-${dato.name}`}>
            <div className={styles["timeline-info"]}>
              <span>{dato.from_time}</span>
            </div>
            <div className={styles["timeline-marker"]} />
            <div className={styles["timeline-content"]}>
              <h3 className={styles["timeline-title"]}>{dato.name}</h3>
            </div>
          </li>
        ))}
      </ul>
    </div>
  )
}
