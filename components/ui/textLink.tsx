import { cn } from "@/lib/utils"
import Link, { LinkProps } from "next/link"
import { ReactNode } from "react"

export const TextLink = (props: Omit<LinkProps, "children"> & { children?: ReactNode }) => {
  return (
    <Link
      {...props}
      target="_blank"
      rel="noreferrer"
      className={cn("no-underline text-link-primary hover:text-link-secondary")}
    />
  )
}
