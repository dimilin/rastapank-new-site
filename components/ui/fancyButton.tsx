import { cn } from "@/lib/utils"
import Link, { LinkProps } from "next/link"
import { ReactNode } from "react"
import styles from "./FancyButton.module.css"

type FancyButtonProps = Omit<LinkProps, "children"> & { children?: ReactNode }

export const FancyButton = (props: FancyButtonProps) => {
  const { children, ...rest } = props

  return (
    <Link
      target="_blank"
      {...rest}
      className={cn(
        styles["fancy-button"],
        "flex justify-center items-center",
        "no-underline w-[3rem] h-[3rem] bg-fancy-white rounded-[30%] shadow-fancy overflow-hidden text-fancy-primary relative",
        "hover:scale-125 hover:text-fancy-white hover:border-none hover:shadow-fancy-blue",
        "before:absolute before:w-[120%] before:h-[120%] before:bg-fancy-primary before:rotate-45 before:left-[-110%] before:top-[90%]",
        "before:hover:animate-fancy-button before:hover:transition-all before:hover:top-[-10%] before:hover:left-[-10%]",
      )}
    >
      <div className={cn(styles["fancy-text"], "relative bottom-0 right-0")}>{children}</div>
    </Link>
  )
}
