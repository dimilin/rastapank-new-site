"use client"

import { cn } from "@/lib/utils"
import Image from "next/image"
import Link from "next/link"
import { NavigationMenu, NavigationMenuItem, NavigationMenuLink, NavigationMenuList } from "./ui/navigationMenu"

export const Nav = (props: { className?: string }) => {
  return (
    <div
      className={cn(
        "flex justify-center w-full h-40 items-center fixed top-0 left-0 right-0 z-50 px-[22vw] bg-transparent",
        props.className,
      )}
    >
      <div className="flex w-full flex-nowrap justify-between items-center">
        <div>
          <Link href="/" className="opacity-90 hover:opacity-100">
            <Image src="/logoweb2.png" alt="logo" width={150} height={107} priority />
          </Link>
        </div>
        <NavigationMenu>
          <NavigationMenuList className="flex items-center gap-12 font-semibold text-xs text-nav">
            <NavigationMenuItem>
              <Link href="/radio" passHref legacyBehavior>
                <NavigationMenuLink>radio</NavigationMenuLink>
              </Link>
            </NavigationMenuItem>
            <NavigationMenuItem>
              <Link href="/chat" passHref legacyBehavior>
                <NavigationMenuLink>chat</NavigationMenuLink>
              </Link>
            </NavigationMenuItem>
            <NavigationMenuItem>
              <Link href="/culture" passHref legacyBehavior>
                <NavigationMenuLink>culture</NavigationMenuLink>
              </Link>
            </NavigationMenuItem>
            <NavigationMenuItem>
              <Link href="/herakleion-diy" passHref legacyBehavior>
                <NavigationMenuLink>herakleion diy</NavigationMenuLink>
              </Link>
            </NavigationMenuItem>
            <NavigationMenuItem>
              <Link href="/about" passHref legacyBehavior>
                <NavigationMenuLink>about</NavigationMenuLink>
              </Link>
            </NavigationMenuItem>
          </NavigationMenuList>
        </NavigationMenu>
      </div>
    </div>
  )
}
