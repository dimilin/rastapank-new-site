"use client"
import Image from "next/image"

type GalleryProps = {
  data: {
    src: string
    id: string
    url: string
    site: string
    logo: string
  }[]
}

export default function Gallery(props: GalleryProps) {
  const { data } = props

  return (
    <div className="images-container">
      {data.map((d) => {
        return <Image key={d.id} src={d.src} height={273} width={273} alt="" onClick={() => window.open(d.site)} />
      })}
    </div>
  )
}
