import { RadioData } from "@/lib/radio/type"
import { cn } from "@/lib/utils"
import Image from "next/image"
import Link from "next/link"

type CardProps = RadioData

export const Card = (props: CardProps) => {
  const { subTitle, title, url, src } = props

  return (
    <div
      className={cn(
        "xl:w-1/5 lg:w-1/4 md:w-1/3 sm:w-1/2",
        "flex flex-col text-center border-t border-t-[#00000008] rounded-xl shadow-radio-card bg-white text-black mb-[20px]",
        "transition-card duration-card ease-card delay-0 ",
        "hover:shadow-radio-card-hover hover:translate-y-[-10px]",
      )}
    >
      <Link href={url || ""} target={url?.startsWith("/") ? undefined : "_blank"}>
        <div className="flex justify-center items-center overflow-hidden py-3 px-5 border-b border-b-[#00000020] bg-[#00000008]">
          <Image width="200" height="200" src={src} alt={title} className="rounded-t-xl w-auto h-auto max-h-[200px]" />
        </div>
        <div className="p-5">
          <div className="text-xl text-[#6c757d] font-light mb-2">{title}</div>
          <div className="text-sm text-[#66615b] pb-4 font-light">{subTitle}</div>
        </div>
      </Link>
    </div>
  )
}
