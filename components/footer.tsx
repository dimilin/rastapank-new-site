import { InfoData } from "@/lib/type"
import { cn } from "@/lib/utils"
import { CopyrightIcon, Facebook, Github, Gitlab } from "lucide-react"
import PocketBase from "pocketbase"
import { FancyButton } from "./ui/fancyButton"
import { TextLink } from "./ui/textLink"

async function getData() {
  const pb = new PocketBase("http://127.0.0.1:8090")
  const data = (await pb.collection("info").getList<InfoData>()).items.at(0)
  return data
}

export default async function Page(props: { className?: string }) {
  const data = await getData()

  if (!data) return null

  return (
    <footer className={cn("text-footer bg-footer-dark", props.className)}>
      <div className="flex flex-wrap p-4 w-3/5 m-auto justify-between gap-y-14">
        <div className="flex flex-col gap-8 justify-start ">
          <div className="flex flex-col gap-y-2">
            <span className="text-base font-semibold">ΔΙΕΥΘΥΝΣΗ</span>
            <span className="text-lg font-light">Πανεπιστημιούπολη Βουτών, Φοιτητικό Κέντρο</span>
          </div>
          <div className="flex flex-col gap-y-2">
            <span className="text-base font-semibold">ΕΠΙΚΟΙΝΩΝΙΑ</span>
            <div className="flex flex-col gap-y-1">
              <span className="text-lg font-light">
                studio: <TextLink href={`callto:${data.tel}`}>{data.displayTel}</TextLink>
              </span>
              <span className="text-lg font-light">
                email: <TextLink href={`mailto:${data.email}`}>{data.email}</TextLink>
              </span>
            </div>
          </div>
        </div>
        <div className="flex flex-col justify-start items-center gap-8 p-8">
          <div className="flex flex-col justify-start items-center gap-2">
            <span className="flex items-center text-xs font-light gap-1">
              <CopyrightIcon size={8} />
              <span>{`${new Date().getFullYear()} Made by rastapank 96.7`}</span>
            </span>
            <span className="text-xs font-light">Το περιεχόμενο του site διατίθεται υπό την άδεια CC BY-NC-SA</span>
          </div>
          <div className="flex items-center justify-center gap-7">
            <FancyButton href={data.facebook}>
              <Facebook />
            </FancyButton>
            <FancyButton href={data.github}>
              <Github />
            </FancyButton>
            <FancyButton href={data.gitlab}>
              <Gitlab />
            </FancyButton>
          </div>
        </div>
      </div>
    </footer>
  )
}
