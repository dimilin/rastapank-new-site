"use client"

import { useRef, useState } from "react"
import ReactPlayer from "react-player"

const radioUrls = ["https://rs.radio.uoc.gr:8001/uoc_256.ogg", "https://rs.radio.uoc.gr:8001/uoc_128.mp3"]

type PlayerState = {
  playing: boolean
  musicCardPlaying: { musicCard: "music-card" | "music-card playing"; playBtn: "play" | "pause" | "loader" }
}

export default function RadioPlayer() {
  const ref = useRef<ReactPlayer | null>(null)

  const [playerState, setPlayerState] = useState<PlayerState>({
    playing: false,
    musicCardPlaying: { musicCard: "music-card", playBtn: "play" },
  })

  const handlePlayPause = () => {
    if (!playerState.playing) {
      console.log(ref.current?.getDuration())
      console.log(ref.current?.getSecondsLoaded())
      //this.player.seekTo(0.99, 'fraction')
      ref.current?.seekTo(ref.current?.getSecondsLoaded() - 1, "seconds")
    }
    setPlayerState((prev) => ({ ...prev, playing: !prev.playing }))
  }

  const handlePlay = () => {
    console.log("onPlay")
    setPlayerState((prev) => ({ ...prev, musicCardPlaying: { musicCard: "music-card playing", playBtn: "pause" } }))
    //this.setState({ playing: true })
  }

  const handlePause = () => {
    console.log("onPause")
    setPlayerState((prev) => ({ ...prev, music_card_playing: { music_card: "music-card", play_btn: "play" } }))
    // this.setState({ playing: false })
  }

  const handleStart = () => {
    setPlayerState((prev) => ({ ...prev, music_card_playing: { music_card: "music-card", play_btn: "loader" } }))
  }

  return (
    <div>
      <div className="w-[300px] h-[300px] shadow-music-card overflow-hidden relative rounded-md my-[10px] mx-auto"></div>
      <div>
        <div onClick={handlePlayPause} className={playerState.musicCardPlaying.playBtn} />
      </div>
      <h2 className="presentation-subtitle text-center"></h2>
      <ReactPlayer
        ref={ref}
        url={radioUrls}
        playing={playerState.playing}
        onReady={() => console.log("onReady")}
        onStart={handleStart}
        onPlay={handlePlay}
        onPause={handlePause}
        onBuffer={() => console.log("onBuffer")}
        // controls
      />
    </div>
  )
}
